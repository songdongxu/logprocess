package setting

import (
	"github.com/go-ini/ini"
	"log"
)

type App struct {
	TimeParseLayout string
	TimeLocation    string
	RegexpPattern   string
	AccessLogPath   string
	Excludes        string
}

var AppSetting = &App{}

type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	Name        string
	TablePrefix string
}

var DbSetting = &Database{}

var cfg *ini.File

func Setup() {
	setup("conf/app.ini")
	return
}

func setup(src string) {
	var err error
	cfg, err = ini.Load(src)
	if err != nil {
		log.Fatalf("setting.Setup, fail to parse 'conf/app.ini': %v", err)
	}

	mapTo("app", AppSetting)
	mapTo("db", DbSetting)
}

// mapTo map section
func mapTo(section string, v interface{}) {
	err := cfg.Section(section).MapTo(v)
	if err != nil {
		log.Fatalf("Cfg.MapTo %s err: %v", section, err)
	}
}
