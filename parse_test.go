package main

import (
	"log"
	"logprocess/pkg/setting"
	"net/url"
	"regexp"
	"strings"
	"testing"
)

func TestParse(t *testing.T) {

	//v := `124.158.157.90 - - [30/Aug/2021:22:10:04 +0800] "GET / HTTP/1.1" 200 1873 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7" "-"`

	v := `39.96.130.44 - - [31/Aug/2021:14:17:48 +0800] "GET /_nuxt/pages/single/_id/index.0c621db.js HTTP/1.1" 200 799 "https://www.ldljwx.com/course/1046" "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36" "140.255.212.18"`

	r := regexp.MustCompile(`([\d\.]+)\s+([^ \[]+)\s+([^ \[]+)\s+\[([^\]]+)\]\s+\"([^"]+)\"\s+(\d{3})\s+(\d+)\s+\"([^"]+)\"\s+\"(.*?)\"\s+`)

	ret := r.FindStringSubmatch(v)
	log.Println(ret, len(ret))

	log.Println("0:", ret[0])
	log.Println("1:", ret[1])
	log.Println("2:", ret[2])
	log.Println("3:", ret[3])
	log.Println("4:", ret[4])
	log.Println("5:", ret[5])
	log.Println("6:", ret[6])
	log.Println("7:", ret[7])
	log.Println("8:", ret[8])
	log.Println("9:", ret[9])

	reqSli := strings.Split(ret[5], " ")
	if len(reqSli) != 3 {
		log.Println("strings.Split fail:", ret[6])
	} else {
		u, err := url.Parse(reqSli[1])
		if err != nil {
			log.Println("url.Parse fail:", err)
		} else {
			log.Println(u)
			var b bool
			excludes := strings.Split(setting.AppSetting.Excludes, "|")
			for _, exclude := range excludes {
				if strings.Contains(u.Path, exclude) {
					b = true
					break
				}
			}
			log.Println(b)
		}
	}

}
